;*****************************************************************
;* Archivo: incia.asm
;* Autor: Ivan Gutierrez
;* Fecha: 12/06/2019
;* Descripción: Rutina de inicialización de puertos e interrupciones
;*
;****************************************************************
#include p18f45k50.inc 		;Incluyo archivo de cabecera con definición de registros propios del microcontrolador


#include config.inc


		GLOBAL inicia		;declaro la rutina como global para ser usada en otros módulos

comienza	CODE				;declaro sección de codigo iempre hay que colocar un etiqueta en este caso "in"
	 

;****** configuro los puertos los valores de configuración se encuentran en conf.inc  ************************

inicia	movlb	.15			;asigno banco 15 al bsr para acceder a los registros ANSEL
		clrf	LATA
		movlw	tris_a		;configuro puerto A
		movwf	TRISA
		movlw	ansel_a
		movwf	ANSELA,1
		
		clrf	LATB
		movlw	tris_b		;configuro puerto B
		movwf	TRISB
		movlw	ansel_b
		movwf	ANSELB,1

		clrf	LATC
		movlw	tris_c		;configuro puerto C
		movwf	TRISC
		movlw	ansel_c
		movwf	ANSELC,1

		clrf	LATD
		movlw	tris_d		;configuro puerto D
		movwf	TRISD
		movlw	ansel_d
		movwf	ANSELD,1
		
		clrf	LATE
		movlw	tris_e		;configuro puerto D
		movwf	TRISE
		movlw	ansel_e
		;movwf	ANSELE,1


;**************************** configuro timer 0 ***********************
			
		bcf		T0CON,T08BIT	;modo 16 bits
		bcf		T0CON,T0CS		;selecciono fuente de reloj interna (48mhz/4)
		bcf		T0CON,PSA		;habilito uso de prescaler
		movlw	b'11111000'
		andwf	T0CON,f
		movlw	b'00000001'
		iorwf	T0CON,f			;configuro prescaler para dividir entre 4
		movlw	tmr_h			;cargo valores iniciales del timer
		movwf	TMR0H
		movlw	tmr_l
		movwf	TMR0L
		
	

				

				
;************* configuro interrupciones **********
    clrf	INTCON			;desactivo cualquier interrupción
   ; bsf		RCON,IPEN		;activo opcion de interuupcion con prioridad
    bsf		INTCON,TMR0IE	;habilito inerrupcion de timer 0
    bcf		T0CON,TMR0ON	;desactivo el timer 0
    ;HABILITANDO INTERRUPCIONES EUSART de recepcion pero no de envio
    MOVLW	IPR1_C	
    ;MOVWF	IPR1			;PRIORIDAD PARA EUSART
    MOVLW	PIE1_C
    ;MOVWF	PIE1
    
    ;bcf		INTCON2,TMR0IP	;asigno prioridad baja a interrupcionde timer 0
    bsf		INTCON,GIEL		;activo inerrupcion de bajo nivel
    bsf		INTCON,GIEH		;activo interrupciones



;************* configuro EUSART **********
    movlw	SPBRG_L
    movwf	SPBRG1
    movlw	SPBRG_H
    movwf	SPBRGH1
    MOVLW	TXSTA_C
    MOVWF	TXSTA1			;TXSTA1 CONFIGURO TRANSMISOR
    MOVLW	RCSTA_C
    MOVWF	RCSTA1			;RCSTA1	CONFIGURO RECEPTOR
    MOVLW	BAUDCON_C
    MOVWF	BAUDCON1			;BAUDCON1 CONFIGURATION

    
   movlb	.0
  
   
		return



		end


