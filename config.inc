
;******** definicion de bits del puerto A ******

#define	segA	RA0	;Segmento A del display 7 seg. (salida dig.)
#define segB	Ra1	;Segmento B del display 7 seg. (salida dig.)
#define segE	RA2	;Segmento E del display 7 seg. (salida dig.)
#define segP	RA3	;Punto decimal del display 7 seg. (salida dig.)
#define segC	RA4	;Segmento C del display 7 seg. (salida dig.)
#define segF	RA5	;Segmento F del display 7 seg. (salida dig.)
#define segG	RA6	;Segmento G del display 7 seg. (salida dig.)
#define sefD	RA7	;Segmento D del display 7 seg. (salida dig.)

#define	tris_a	b'00000000'	;configuración direcion de puerto a
#define ansel_a	b'00000000'	;configuración modo puerto a



;******** definicion de bits del puerto B ******

#define	aux0	RB0	;salida auxiliar 0
#define	aux1	RB1	;salida auxiliar 1
#define	aux2	RB2	;salida auxiliar 2
#define	aux3	RB3	;salida auxiliar 3
#define	LCDE	PORTB,RB3	;enable LCD

#define	aux4	RB4	;salida auxiliar 4
#define	LCDRS	PORTB,RB4	;sel. modo LCD

#define	tris_b	b'11100000'	;configuración direcion de puerto b
#define ansel_b	b'00000000'	;configuración modo puerto b



;******** definicion de bits del puerto C ******

#define	aux5	RC0	;salida auxiliar 5
#define cat4	RC1	;catodo 4 del display 7 seg. (salida dig.)
;RC2 no conectado
#define serTX	RC6	;salida comunicación serie (salida dig.)
#define serRX	RC7	;entrada comunicación serie (entrada dig.)


#define	tris_c	b'10111101'	;configuración direcion de puerto c
#define ansel_c	b'00000000'	;configuración modo puerto c

;******** definicion de bits del puerto D ******

#define tecc1	RD0	;Col. 1 del teclado(entrada dig.)
#define tecc2	RD1	;Col. 2 del teclado(entrada dig.)
#define tecc3	RD2	;Col. 3 del teclado(entrada dig.)
#define tecc4	RD3	;Col. 4 del teclado(entrada dig.)

#define LCDD4	RD4	;Entrada datos LCD D4 (salida dig.)
#define LCDD5	RD5	;Entrada datos LCD D5(salida dig.)
#define LCDD6	RD6	;Entrada datos LCD D6(salida dig.)
#define LCDD7	RD7	;Entrada datos LCD D7(salida dig.)


#define	tris_d	b'00001111'	;configuración direcion de puerto d
#define ansel_d	b'00000000'	;configuración modo puerto d


;******** definicion de bits del puerto E ******

#define tecf1	RE0	;Fila 1 del teclado (salida dig.)
#define tecf2	RE1	;Fila 2 del teclado (salida dig.)
#define tecf3	RE2	;Fila 3 del teclado (salida dig.)

#define	cat3	RE0	;Catodo 3 del display 7 seg.(salida dig.)
#define cat2	RE1	;Catodo 2 del display 7 seg.(salida dig.)
#define cat1	RE2	;Catodo 1 del display 7 seg.(salida dig.)


#define	tris_e	b'00000000'	;configuración direcion de puerto d
#define ansel_e	b'00000000'	;configuración modo puerto d	


;************************* valor inicial del timer 0 *************
tmr_l	equ	1fh		;4mS
tmr_h	equ	0d1h


;******** definicion de bits del EUSART ******

#define	TXSTA_C		b'10010011'	;configuración DEL TRANSMISOR
#define RCSTA_C		b'10000111'	;configuración DEL RECEPTOR
#define	BAUDCON_C	b'00001100'	;configuración DEL BAUDRATE GENERATOR
#define	IPR1_C		b'00100000'	;configuración INTERRUPCION DE BAJA PRIORIDAD PARA EUSART
#define PIE1_C		b'00000000'	;configuración INTERRUPCION EUSART
;////////////////////////////////////////
#define SPBRG_L		b'01110000'		;velocidad de comunicacnión a 19.2kHZ
#define SPBRG_H		b'00000010'		;VELOCIDAD DE COMUNICACION A 19.2kHZ   
;/////////////////////////////////////////////
