;*****************************************************************
;* Archivo: main.asm					    *
;* Autor: AGUSTIN NEGRIN				    *
;* Fecha: 28/07/2019					    *
;* Descripci�n: PROGRAMA PARA LABORATORIO DE PROYECTOS,	    *
;*		INSTRUMENTACI�N DE CELDA DE CARGA	    *
;*							    *
;****************************************************************
    
    #INCLUDE p18f45k50.inc 		;Incluyo archivo de cabecera con definici�n de registros propios del microcontrolador
    #INCLUDE config.inc
    #DEFINE mepic			;etiqueta para la complicaci�n en caso de usar la tarjeta mepic	
    
var0	UDATA	0x60
w_2		res	1		;w secundario para ser usado en rutina de interrupci�n
stat_2	res	1		;status secundario para ser usado en rutina de interrupci�n
bsr_2	res	1		;bsr secundario para ser usado en rutina de interrupci�n
var1 	res 1		;definicion de registro para uso general
var2 	res 1 		;definicion de registro para uso general
var3 	res 1		;definicion de registro para uso general
rota 	res 1
dis1	res 1		;digito 1 del 7 segementos
dis2	res 1		;digito 2 del 7 segementos
dis3	res 1		;digito 3 del 7 segementos
dis4	res 1		;digito 4 del 7 segementos
tecpul	res 1
tecla	res 1
tenvi	res	1		;variable para transmision EUSART
ret1		RES	    1		;CONTADOR RETARDO 1
ret2		RES	    1		; CONTADOR RETARDO 2
ret3		RES	    1		; CONTADOR RETARDO 3
VAR1	res	    1
CORRECCION  RES	2		;CORRECCION DE MEDICI�N DE BALANZA(SE ACTUALIZA SOLO AL SETEAR EL CERO Y AL RESETEAR EL PROGRAMA)
BALANZA	RES	3		;LO QUE MIDE LA BALANZA (rEGISTRO SIEMPRE SERA ACTUALIZADO)
R5HI	RES	1
RESULT	RES	3	;RESULTADO DE LA DIVISI�N
REMA	RES	2	;RESTO DE LA DIVISION
DIVI	RES	2	;DENOMINADOR (ESCALADOR)
NUM	RES	3
MINUENDO    RES	3
SUSTRAENDO  RES	3
NUMERADOR   RES	3
DENOMINADOR	    RES	3
RESTO	    RES	3
MODREG1		RES	    1		;CONTADOR RETARDO 1
MODREG0		RES	    1		; CONTADOR RETARDO 2
DIVREG0	    RES	    1
DIVREG1	    RES	    1
TEMP1	    RES	    1
TEMP0	    RES	    1
COUNT	    RES	    1
contador res 1		;Contador
temp	res 1
var4	res 1		;registro para uso general
tauxh	res 1
tauxl	res 1
escala	res 1		   ;tendra en que escala estamos 
k		EQU	0x4		;constante de retardo
n		EQU	0x1			;constante de retardo
    

    GLOBAL w_2,stat_2,bsr_2,rota,contador,var1,var2,var3,var4,dis1,dis2,dis3,dis4,tauxh,tauxl,tecla,tecpul ;defino variables globales que van  ser usadas en otros m�dulos
    
    EXTERN	inicia	;declarci�n como externa de la rutina inicia
	
    IFDEF	 mepic		;verifico si la compilaci�n es para usr en el board pinguino
		
res_mep	CODE	01000h		;vector de reset tarjeta mepic
	goto	inicio
		
fir	CODE	01006h		;firma usada por el bootloader
	DATA 	0x600d	

vl_mep	CODE	01008h		;vector alta prioridad de la tarjeta mepic
		goto	vec_l
	
		ELSE
				 
rs_t	CODE	00h			;vector de reset 
		goto	inicio		;salto al inicio de mi programa
	
vl	CODE	0008h		;vector alta prioridad 
		goto	vec_l
		
    endif
	
main	code	01100h
	 
;******************** inicio del programa principal *********************

inicio						;inicio del programa principal
		movlb	00h			;selecciono banco de registro 0
		clrf	INTCON			;limpio INTCON
		
		ifdef	mepic		;en esta seccion configuro el oscilador para trabajar en 48MHZ
		movlw	0x70		;solo cuando voy a usar la tarjeta mepic
		movwf	OSCCON
		movlw	0x10
		movwf	OSCCON2
		movlw	0x80
		movwf	OSCTUNE
tune		btfss	OSCCON2,PLLRDY	;espero a que el oscilador se estabilice
		goto	tune
		endif
		
    call	inicia		;inicio los puertos y configuro interrupciones
    clrf	LATB
    bcf		LATC,6
    banksel	dis1
    movlw	.16			;w=16
    movwf	dis1,1			;dis1=w = 0(tabla s7)
    movwf	dis2,1			;dis2=w = 0(tabla s7)
    movwf	dis3,1			;dis3=w = 0(tabla s7)
    movwf	dis4,1			;dis4=w = 0(tabla s7)
     BANKSEL	rota
    clrf	rota,1		;inicio rl registro de rotaci�n de los display 7 segmentos
    clrf	tecpul,1
    clrf	tecla,1
     BANKSEL	var4
   CLRF		var4,1
   BANKSEL	var3
   CLRF		var3,1
   BANKSEL	CORRECCION
    clrf	CORRECCION+1
    CLRF	CORRECCION
   BANKSEL	BALANZA
   clrf		BALANZA+2
    CLRF	BALANZA+1
    CLRF	BALANZA
    bsf		T0CON,TMR0ON	;activo el timer 0
    bsf		INTCON,TMR0IE		;activo interrupciones
    BSF		INTCON,5
    setf	LATB
    ;Espero mas o menos 500ms para porsiacaso el HX711 todavia no esta listo (power on reset settling time)
    movlw	0xFF
    CALL	retardo
    movlw	0xFF
    CALL	retardo
    CALL	LEE_BAL
    ;CORRIGO EL CERO ;NO ME INTEREZA EL PRIMER BYTE (REDUZCO RUIDO)
    movff	BALANZA+2, CORRECCION+1
    MOVFF	BALANZA+1, CORRECCION
    
    ;ciclo infinito de espera a tecla pulsada
aqui	movf	tecpul,1,1		;tecpul=tecpul (afecta registro STATUS)
		btfsc	STATUS,Z	;tecpul es cero? 
		goto	aqui		;NO! sigo esperando
		;Si estamos aqui es porq pulsaste una tecla
		banksel	tecla
		movlw	.1
		cpfsgt	tecla,1
		call	CAPTURA
		movlw	.1
		cpfsgt	tecla,1
		goto	libtec
		;otra tecla resetea el cero
		CALL	LEE_BAL
		;CORRIGO EL CERO ;NO ME INTEREZA EL PRIMER BYTE (REDUZCO RUIDO)
		movff	BALANZA+2, CORRECCION+1
		MOVFF	BALANZA+1, CORRECCION
		CALL	CAPTURA
		
		
libtec		
		BANKSEL	tecpul			;espero a que se libere la tecla preguntando por tecpul
		movf	tecpul,1,1
		btfss	STATUS,Z
		bra	libtec
		bra	aqui
CAPTURA	
	CALL	LEE_BAL			    ;LEEO EL HX711
	;CORRIGO EL ZERO (LO QUE ME DIO LA BALANZA MENOS EL CERO PREVIAMENTE OBTENIDO)
	;NO ME INTEREZA EL PRIMER BYTE (REDUZCO RUIDO)
	MOVFF	BALANZA+2, MINUENDO+1	    ;LO QUE ME DE EL HX711 SER� MI MINUENDO
	MOVFF	BALANZA+1, MINUENDO
	MOVFF	CORRECCION, SUSTRAENDO	    ;LO QUE TENGA EN CORRECCI�N SERA MI SUSTRAENDO
	MOVFF	CORRECCION+1, SUSTRAENDO+1
	CALL	RESTA			    ;RESTO BALANZA-CERO
	;tOCA cALIBRAR mEDICI�N
	MOVFF	MINUENDO+1, NUMERADOR+1
	MOVFF	MINUENDO,   NUMERADOR
	MOVLW	B'01110101'	    ;CONSTANTE DE CALIBRAC�N: .117  Para la de 20Kgs es .3 o B'00000011'
	MOVWF	DENOMINADOR
	CLRF	DENOMINADOR+1
	call	DIV
	MOVLW	.10		    ;PARA DIVIDIR ENTRE 10 Y OBTENER LOS DIGITOS DISPLAY
	MOVWF	DENOMINADOR
	CLRF	DENOMINADOR+1
	CALL	INTTOSTR
	RETURN
	
	
INTTOSTR
	CALL	DIV
	MOVFF	RESTO,    dis4
	CALL	DIV
	MOVFF	RESTO,    dis3
	CALL	DIV
	MOVFF	RESTO,    dis2
	CALL	DIV
	MOVFF	RESTO,    dis1
	
	banksel	dis4
	movlw	.16
	addwf	dis4,F,1
	addwf	dis3,F,1
	addwf	dis2,F,1
	addwf	dis1,F,1
	RETURN
	
	

DIV
	call    setup
       clrf    RESTO+1
       clrf    RESTO
dloop   bcf     STATUS,C
       rlCf     RESULT, F
       rlCf     RESULT+1, F
       rlCf     RESTO, F
       rlCf     RESTO+1, F

       ;Comparing MSB
       movf    DENOMINADOR+1,W
       subwf   RESTO+1,W        ;C-A    check if a>c
       btfss   STATUS,Z        ;if c>=a then check LSB
       goto    nochk

       ;Comparing LSB
       movf    DENOMINADOR,W
       subwf   RESTO,W        ;ACCcLO-ACCaLO
                               ;if msb equal then check lsb
nochk   btfss   STATUS,C        ;if c>a carry set, therefore do this
       goto    nogo            ;therefore all c > all a, dont

       ;subtracting LSB
       movf    DENOMINADOR,W        ;c-a into c
       subwf   RESTO, F
       ;put result into ACCcLO

       ;if C-LSB > A-LSB then dec ACCaHI
       btfss   STATUS,C
       decf    RESTO, F

       ;substracting MSB
       movf    DENOMINADOR+1,W
       subwf   RESTO+1, F
       ;put result into ACCcHI

       bsf     STATUS,C    ;shift a 1 into b (result)
nogo    rlCf     NUMERADOR, F
       rlCf     NUMERADOR+1, F
       decfsz  temp, F         ;loop untill all bits checked
       goto    dloop
;
       retlw   0
       
   ;*******************************************************************
;
setup   movlw   .16             ; for 16 shifts
	movwf	temp
       movf    NUMERADOR+1,W          ;move ACCb to ACCd
       movwf   RESULT+1
       movf    NUMERADOR,W
       movwf   RESULT
       clrf    NUMERADOR+1
       clrf    NUMERADOR
       retlw   0
	
;SUBRUTINA QUE RESTA NUMEROS DE 16 BITS, RESULTADO ES DADO EN EL MINUENDO		
RESTA
	BANKSEL	SUSTRAENDO
	movf    SUSTRAENDO,W
        subwf   MINUENDO,F
        movf    SUSTRAENDO+1,W
        subwfb  MINUENDO+1,F
	RETURN

LEE_BAL

    BANKSEL	BALANZA
    btfsc	PORTC,7		    ;ESPERO A QUE EL HX711 ESTE LISTO PARA TRANSMITIR
    BRA		LEE_BAL
    BSF		RCSTA1,CREN	    ;CONTINUOS RECEIVER BIT
D1  BTFSS	PIR1,RCIF	    ;RECIBI?
    BRA		D1
    MOVFF	RCREG1, BALANZA+2    ;TSBYTE 
    BCF		PIR1,RCIF
    BSF		LATA,0
D2  BTFSS	PIR1,RCIF	    ;RECIBI?
    BRA		D2
    MOVFF	RCREG1, BALANZA+1    ;MSBYTE 
    BCF		PIR1,RCIF
    BSF		LATA,1
D3  BTFSS	PIR1,RCIF	    ;RECIBI?
    BRA		D3
    BCF		RCSTA1,CREN	    ;DEJO DDE RECIBIR
    MOVFF	RCREG1, BALANZA      ;LSBYTE 
    BSF		LATC,6		    ;LANZO EL ULTIMO PULSO
    MOVLW	.135		    ;ESPERARE 220 CICLOS DE RELOJ PARA BAJAR PULSO
    banksel	var2
    MOVWF	var2		    ;2ic
LOOPI	DECFSZ	var2,f		    ;2* var2 +3 ic 
    BRA		LOOPI
    BCF		LATC,6		    ;BAJO PULSO
    RETURN
	
retardo						;rutina que genera un retardo de W x 1mS aprox.
		BANKSEL	ret1
		movwf	ret1,1		;en W se encuentra el valor que define el retardo
	
lazo2	
		movlw	k			;Asigno valor inicial a var2
	
		movwf	ret2,1		;Decremento Var1
	
lazo1	nop
		nop
		nop
		movlw	n			;asigno valor inicial a var3
		movwf	ret3,1		;decremento var3
		
lazo3	decfsz	ret3,1,1
		bra		lazo3


		decfsz	ret2,1,1
		bra		lazo1		;Si var2 no es cero decremento de nuevo
		decfsz	ret1,1,1	;var2 es cero decremento var1
		bra		lazo2		;Si var1 no es cero repito loop1
    return				;retorno de la subrutina

;************* interrupciones*********

vec_l	
		;SETF	LATB 
		movwf	w_2				;salvo el valor de w
		movff	STATUS,stat_2	;salvo el valor de STATUS
		movff	BSR,bsr_2		;salvo el valor de BSR

		movlw	tmr_h			;cargo valores iniciales del timer
		movwf	TMR0H
		movlw	tmr_l
		movwf	TMR0L	
		;SETF	LATB 
		BANKSEL	rota			;verifico cual dislay encender
		
;		tstfsz	var3
;		call	CAPTURA
		
		movlw	00h
		cpfseq	rota,1			;verifico el valor del registro rota paa saber cual display se debe encender
		goto	ver_d2		
		incf	rota,1,1		;incremento registro de rotaci�n apra apuntr al pr�ximo dispay
		movlw	b'00000100'		;selecciono el display activando el catodo del mismo y desactvando los demas
		movwf	LATE	
		bcf		LATC,cat4
		BANkSEL	dis1			;asigno banco de registros
		movf	dis1,w,1		;cargo el valor a presentar en el dsplay
		call	tab_dis			;busco el valor en la tabla de digitos
		movwf	LATA			;coloco el valor en el puerto A
		
;****************************************** verifico teclado   ************
		bcf	tecpul,0,1		;indico que no hay tecla pulsada
		movf	PORTD,w			;verifico si el Puerto D est� en 0
		andlw	0Fh
		btfsc	STATUS,Z
		goto	fin_int			;no hay tecla pulsada en la fila uno
		movlw	0x9
		BANKSEL	tecla
		movwf	tecla,1
		call	r_col
		bsf		tecpul,0,1		;indico que hay tecla pusada en esta fila
;***************************************************************


		goto	fin_int			;salgo de la interrupci�n

ver_d2	movlw	01h
		cpfseq	rota,1			;verifico el valor del registro rota para saber cual display se debe encender
		goto	ver_d3		
		incf	rota,1,1		
		movlw	b'00000010'
		movwf	LATE
		bcf		LATC,cat4
		BANkSEL	dis2			;asigno banco de registros
		movf	dis2,w,1		;cargo el valor a presentar en el dsplay
		call	tab_dis			;busco el valor en la tabla de digitos
		movwf	LATA			;coloco el valor en el puerto A
;****************************************** verifico teclado   ************
		bcf		tecpul,1,1		;indico tecla no hay pulsada en esta fila
		movf	PORTD,w			;verifico si el Puerto D est� en 0
		andlw	0Fh
		btfsc	STATUS,Z
		goto	fin_int			;no hay tecla pulsada en la fila uno
		movlw	0x5
		BANKSEL	tecla
		movwf	tecla,1
		call	r_col
		bsf		tecpul,1,1		;indico que hay tecla pusada en esta fila
;***************************************************************
		goto	fin_int			;salgo de la interrupci�n


ver_d3	movlw	02h
		cpfseq	rota,1
		goto	ver_d4		
		incf	rota,1,1		;incremento registro de rotaci�n apra apuntr al pr�ximo dispay
		movlw	b'00000001'
		movwf	LATE
		bcf		LATC,cat4
		BANkSEL	dis3			;asigno banco de registros
		movf	dis3,w,1		;cargo el valor a presentar en el dsplay
		call	tab_dis			;busco el valor en la tabla de digitos
		movwf	LATA			;coloco el valor en el puerto A
;****************************************** verifico teclado   ************
		bcf		tecpul,2,1		;indico tecla no hay pulsada en esta fila
		movf	PORTD,w			;verifico si el Puerto D est� en 0
		andlw	0Fh
		btfsc	STATUS,Z
		goto	fin_int			;no hay tecla pulsada en la fila uno
		movlw	0x1
		BANKSEL	tecla
		movwf	tecla,1
		call	r_col
		bsf		tecpul,2,1		;indico que hay tecla pusada en esta fila
;***************************************************************
		goto	fin_int			;salgo de la interrupci�n


ver_d4	movlw	03h
		cpfseq	rota,1
		goto	no_dis		
		clrf	rota,1			;secci�n de encendido del display 4
		movlw	b'00000000'
		movwf	LATE
		bsf		LATC,cat4
		BANkSEL	dis1			;asigno banco de registros
		movf	dis4,w,1		;cargo el valor a presentar en el dsplay
		call	tab_dis			;busco el valor en la tabla de digitos
		movwf	LATA			;coloco el valor en el puerto A
;****************************************** verifico teclado   ************
		bcf		tecpul,3,1		;indico tecla no hay pulsada en esta fila
		movf	PORTD,w			;verifico si el Puerto D est� en 0
		andlw	0Fh
		btfsc	STATUS,Z
		goto	fin_int			;no hay tecla pulsada en la fila uno
		movlw	0xd
		BANKSEL	tecla
		movwf	tecla,1
		call	r_col
		bsf		tecpul,3,1		;indico que hay tecla pusada en esta fila
		
;***************************************************************			

no_dis 	clrf	rota,1			;inicio contador de rotaciones

tab_dis	
		BANKSEL var1			;rutina que lee los valores asgnados a la tala de digitos del display
		movwf	var1,1			;salvo el valor del digito a mostrar
		movlw	high	s7_tab_ascii		;selecciono msb de la direccion de la tabla
		movwf	tauxh			;asigno el valor al reg. msb del apuntador de tabla
		movlw	low	s7_tab_ascii		;selecciono lsb de la direccion de tabla
		movwf	tauxl			;asigno valor lsb al reg. de apuntador de tabla
		movf	var1,w,1		;recuepro el valor del digito a mostrar
		addwf	tauxl,f,1		;lo sumo a la direcci�n de la tabla para apuntar al valor correspondiente
		btfsc	STATUS,C
		incf	tauxh,1,1
		movf	tauxl,w,1
		movwf	TBLPTRL			;asigno los valores con el offset para apuntar al dato
		movf	tauxh,w,1
		movwf	TBLPTRH
		tblrd*					;leo el dato de la tabla
		movf	TABLAT,w		;lo devuelvo en w
		return	


r_col			
		movlw	.5
		banksel	contador		;estas 3 lineas de codigo me reinician el contador a 5
		movwf	contador,1		
		btfss	PORTD,tecc1		;verifico si hay ua tecla en la columna 1
		goto	ver_c2			;verifico si est� en la columna 2
		return					;hay una tecla pulsada en la columna uno no sumo ningun offset
ver_c2	btfss	PORTD,tecc2
		goto	ver_c3      	;verifico si est� en la columna 3
		movlw	.1				;est� en la columna 2 sumo el offset de columna a la tecla pulsada
		addwf	tecla,1
		return
ver_c3	btfss	PORTD,tecc3
		goto	ver_c4			;verifico si est� en la columna 4
		movlw	.2				;est� en la columna 3 sumo el offset de columna a la tecla pulsada
		addwf	tecla,1
		return
ver_c4	btfss	PORTD,tecc4
		goto	no_col			;no esta en ninguna columna salgo de la rutina		
		movlw	.3				;est� en la columna 4 sumo el offset de columna a la tecla pulsada
		addwf	tecla,1
		return
no_col	clrf	tecla,1
		return		

		
fin_int		movf	w_2,w			;recupero w
		movff	bsr_2,BSR		;recupero BSR
		movff	stat_2,STATUS	;recupero STATUS
		bcf		INTCON,TMR0IF	;limpio bandera de interrupci�n de timer 0
		retfie					;retorno de la interrupci�n	
	
s7_tab_ascii				;tabla con los valores para enceder los segmentos correspondientes a cada n�mero
		
	    ;  1  0   3  2    5 4     7 6      9 8    11 10  1312       14      16      18    20      22      24      26      28     30    3332   3534     36       38      40    42        44      46     48      50      52      54      56    5 8     60     6 2    6564    6766     68     70    72    74        7 6       78     8 0     8 2       84    8 6      88     9 0    92      9 4    
	data 0x0000,0x0000, 0x4Ef1, 0x2044, 0x83a1, 0x6421,0x4004, 0x4608, 0x12b7, 0xd3c7,0xf172, 0x13f5, 0xF3F7, 0x9181, 0xc061,0x4f43, 0x77d7,0xa5f4, 0xe5d6, 0xf565, 0x2476, 0x7596, 0x15a4, 0xb737,0xE367, 0xf150, 0xb6e4, 0xA2b6, 0xf276, 0xa5c7,0x9370,0x8023, 0xd702, 0xC4f4,0xe7d6, 0xF365, 0x0474, 0x7590, 0x1424, 0xd454, 0x7367, 0xf144, 0x94e4, 0x1494, 0xf276, 0x52c7, 0x6424, 0x0094
	
;0	0x00, /* (space) */0x86, /* ! *
;2	0x22, /* " */0x7E, /* # */
;4	0x6D, /* $ */0xD2, /* % */
;6	0x46, /* & */0x20, /* ' */
;8	0x29, /* ( */0x0B, /* ) */
;10	0x21, /* * */0x70, /* + */
;12	0x10, /* , */0x40, /* - */
;14	0x80, /* . */0x52, /* / */
;16	0x3F, /* 0 */0x06, /* 1 */
;18	0x5B, /* 2 */0x4F, /* 3 */
;20	0x66, /* 4 */0x6D, /* 5 */
;22	0x7D, /* 6 */0x07, /* 7 */
;24	0x7F, /* 8 */0x6F, /* 9 */
;26	0x09, /* : */0x0D, /* ; */
;28	0x61, /* < */0x48, /* = */
;30	0x43, /* > */0xD3, /* ? */
;32	0x5F, /* @ */0x77, /* A */
;34 35	0x7C, /* B */0x39, /* C */
;36 37	0x5E, /* D */0x79, /* E */
;38	0x71, /* F */0x3D, /* G */
;40	0x76, /* H */0x30, /* I */
;42	0x1E, /* J */0x75, /* K */
;44	0x38, /* L */0x15, /* M */
;46	0x37, /* N */0x3F, /* O */
;48	0x73, /* P */0x6B, /* Q */
;50	0x33, /* R */0x6D, /* S */
;52	0x78, /* T */0x3E, /* U */
;54	0x3E, /* V */0x2A, /* W */
;56	0x76, /* X */0x6E, /* Y */
;58	0x5B, /* Z */0x39, /* [ */
;60	0x64, /* \ */0x0F, /* ] */
;62	0x23, /* ^ */0x08, /* _ */
;64	0x02, /* ` */0x5F, /* a */
;66	0x7C, /* b */0x58, /* c */
;68	0x5E, /* d */0x7B, /* e */
;70	0x71, /* f */0x6F, /* g */
;72	0x74, /* h */0x10, /* i */
;74	0x0C, /* j */0x75, /* k */
;76	0x30, /* l */0x14, /* m */
;78	0x54, /* n */0x5C, /* o */
;80	0x73, /* p */0x67, /* q */
;82	0x50, /* r */0x6D, /* s */
;84	0x78, /* t */0x1C, /* u */
;86	0x1C, /* v */0x14, /* w */
;88	0x76, /* x */0x6E, /* y */
;90	0x5B, /* z */0x46, /* { */
;92	0x30, /* | */0x70, /* } */
;94	0x01, /* ~ */0x00, /* (del) */
	
	end